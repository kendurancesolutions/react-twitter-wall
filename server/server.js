// https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/quick-start
const http      = require('http')
const path      = require('path')
const express   = require('express')                // https://expressjs.com/
const socketIO  = require('socket.io')              // https://socket.io/
const needle    = require('needle')                 // https://github.com/tomas/needle
const config    = require('dotenv').config()        // https://github.com/motdotla/dotenv
const cors      = require('cors')  // https://developer.okta.com/blog/2021/07/14/socket-io-react-tutorial
const bodyParser = require('body-parser')
const {data} = require("autoprefixer");  // https://stackoverflow.com/questions/9177049/express-js-req-body-undefined
const jsonParser = bodyParser.json()

const TOKEN     = process.env.TWITTER_BEARER_TOKEN  // https://developer.twitter.com/
const PORT      = process.env.PORT_SERVER || 3000

const app       = express()
const server    = http.createServer(app)
const io        = socketIO(server,{
	cors: {
		origin: '*',
		methods: ['GET', 'POST']
	}
})

app.use(cors())

const _url      = {
	rules:          "https://api.twitter.com/2/tweets/search/stream/rules",
	stream:         "https://api.twitter.com/2/tweets/search/stream",
	search_recent:  "https://api.twitter.com/2/tweets/search/recent",
	search_all:     "https://api.twitter.com/2/tweets/search/all",
	filters:      {
		'tweet.fields': `attachments,author_id,text,possibly_sensitive,created_at,id,source,lang`,
		'expansions':   `attachments.media_keys,referenced_tweets.id,author_id`,
		'user.fields':  `username,name,profile_image_url`,
		'media.fields': `preview_image_url,url,alt_text`,
	}
}

/****************** Backend ******************/
/** Get stream rules **/
const getRules    = async () => {
	const { body }  = await needle(`GET`, _url.rules, {
		headers: {
			Authorization: `Bearer ${TOKEN}`,
		}
	})
	return body.data
}
/** Set stream rules **/
const setRules = async (rules) => {
	const data = {
		add: rules
	}
	const response = await needle(`POST`, _url.rules, data, {
		headers: {
			Authorization:  `Bearer ${TOKEN}`,
			'content-type': `application/json`,
		}
	})
	return response.body
}
/** Delete stream rules **/
const deleteAllRules = async (rules) => {
	if(!Array.isArray(rules)) return null
	const data = {
		delete: { ids: rules.map((rule) => rule.id) }
	}
	const response = await needle(`POST`, _url.rules, data, {
		headers: {
			Authorization:  `Bearer ${TOKEN}`,
			'content-type': `application/json`,
		}
	})
	return response.body
}

const streamTweets = async (socket) => {
	const currentRules = await getRules()
	console.log(`Listening for tweets with the following rules ${JSON.stringify({_rules: currentRules})}...`)
	const url = `${_url.stream}?${(new URLSearchParams(_url.filters).toString())}`
	const stream = needle.get(url, {
		headers: {
			Authorization: `Bearer ${TOKEN}`
		},
	})
	stream.on('data', (data) => {
		try{
			const tweet = JSON.parse(data)
			socket.emit('tweet', tweet)
		}catch(e){ }
	})
	return stream
}
const searchTweets    = async ( currentRules, { max_results, pagination_token } ) => {
	/**
	 body: { pagination_token: null, max_results: 3 },
  pagination_token: null,
  max_results: 3
	 */
	try{
		/**
	 stream url = url: 'https://api.twitter.com/2/tweets/search/stream?                    tweet.fields=attachments,author_id,text,possibly_sensitive,created_at,id,source,lang&expansions=attachments.media_keys,referenced_tweets.id,author_id&user.fields=username,name,profile_image_url&media.fields=preview_image_url,url,alt_text'
	 search url = url: 'https://api.twitter.com/2/tweets/search/recent?query="remote jobs"&tweet.fields=attachments,author_id,text,possibly_sensitive,created_at,id,source,lang&expansions=attachments.media_keys,referenced_tweets.id,author_id&user.fields=username,name,profile_image_url&media.fields=preview_image_url,url,alt_text'
		 */
		const ruleString = currentRules && currentRules?.[0].value
		const filters = {query:ruleString, ..._url.filters}
		const paginationString = `max_results=${max_results}&${pagination_token && `next_token=${pagination_token}&`}`
		const url = `${_url.search_recent}?${paginationString + (new URLSearchParams(filters).toString())}`
		const { body }  = await needle(`GET`, url, {
			headers: {Authorization: `Bearer ${TOKEN}`, },
		})
		return body
	}catch(error){
		console.log(`searchTweets(): error = ${error}`)
	}
}

const check_delete_set_rules = async ( currentRules, newRules ) => {
	const isTheSame = compareOldNewRules(currentRules, newRules).isTheSame
	if(!isTheSame){
		await deleteAllRules(currentRules)
		await setRules(newRules)
	}
}

io.on('connection', async (socket) => {
	console.log(`Client connected..`)
	// Begin tweet stream
	const filteredStream = await streamTweets(io)  // Determine how to slow it down perhaps?
	// Handle timeout
	let timeout = 0
	filteredStream.on('timeout', () => {
		// Reconnect on error
		console.warn('A connection error occurred. Reconnecting...')
		setTimeout(() => {
			timeout++
			streamTweets(io)
		}, 2 ** timeout)
		streamTweets(io)
	})

	socket.on("disconnect", () => {
		console.log("client manually disconnected")
		socket.disconnect(0)
		filteredStream.destroy() // may not be the standard way
	})
})


/** Utilities **/
const compareOldNewRules = (currentRules, newRules) => {
	if(currentRules) currentRules = currentRules.map((rule) => {return {value:rule.value}})
	return {
		isTheSame: comparer_objArray(currentRules, newRules)
	}
}
const comparer_objArray = (a,b) => {
	const arraysExist     = a && b
	const bothAreArrays   = Array.isArray(a) && Array.isArray(b)
	const bothSameSize    = arraysExist && a.length === b.length
	const asAreInBs       = arraysExist && a.every((val) => b.some(item => item.value === val.value))
	const bsAreInAs       = arraysExist && b.every((val) => a.some(item => item.value === val.value))
	return  bothAreArrays &&  // both are arrays
					bothSameSize  &&  // both are the same size
					asAreInBs     &&  // everything in a is in b
					bsAreInAs         // everything in b is in a
}

/****************** For frontend ******************/
/** Fetch rules **/
app.get('/api/rules', async (req, res) => {
	const currentRules  = await getRules()
	res.status(200).json(currentRules)
})
/** Change rules **/
app.post(`/api/rules`, jsonParser, async ( req,res) => {
	const currentRules  = await getRules()
	const { payload }   = req.body
	const newRules      = [ {value: payload}, ]
	await check_delete_set_rules(currentRules, newRules)
	res.status(200).json(payload)
})
/** Search tweets **/
app.post(`/api/search`, jsonParser, async ( { body },res) => {
	const currentRules  = await getRules()
	const results       = await searchTweets(currentRules, body)
	res.status(200).json(results)
})

/** Start listening **/
server.listen(PORT, () => {
	console.log(`Listening on PORT ${PORT}`)
})
