# React Twitter Wall

A recreation of the CRI Tagboard app now using React.

### Sources
- Original CRI Tagboard - http://w3.georgiasouthern.edu/cri/tagboard/

- Traversy Media - https://github.com/bradtraversy/real-time-tweet-stream

- Twitter Developer Platform - https://developer.twitter.com/en/docs/tutorials/building-an-app-to-stream-tweets

- Twitter Rule Examples - https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/integrate/build-a-rule#examples

- Styled Components - https://styled-components.com/

- React Hooks - https://reactjs.org/docs/hooks-reference.html

- Masonry - https://github.com/eiriklv/react-masonry-component

### Remaining TODO Items
- Adjust column width for mobile screens

- Enable fading/transitions for new tweets

- Add infini-scroll for "Load More"