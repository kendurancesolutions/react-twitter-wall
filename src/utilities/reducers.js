/** Main Reducer **/
export const stateReducer = (state, action) => {
	switch(action.type){
		case "ADD_TWEET": {
			const tweets = [...state.tweets, action.payload]
			return {...state, tweets:tweets}
		}
		case "PREPEND_TWEET": {
			const tweets = [action.payload, ...state.tweets]
			return {...state, tweets:tweets}
		}
		case "CONNECT": {
			let socket = action.payload
			return {...state, socket:socket}
		}
		case "DISCONNECT": {
			let socket = state.socket
			if(socket) {
				console.log('disconnecting from server..')
				socket.disconnect(true)
			}
			return {...state, socket:null}
		}
		case "LOAD_PREVIOUS_TWEETS":{
			const list_ids = state.tweets.map(tweet => { return tweet.tweet_id})
			const newTweets = action.payload.filter( it => { // testing pagination
				return list_ids.indexOf(it.tweet_id) < 0
			})
			const tweets = [ ...state.tweets, ...newTweets ]
			return {...state, tweets:tweets}
		}
		case "UPDATE_PAGINATION_TOKEN":{
			return {...state, paginationOptions: {...state.paginationOptions, pagination_token: action.payload}}
		}
		case "UPDATE_APP_MESSAGE":{
			return {...state, appMessage:action.payload}
		}
		default: return state
	}
}

/** Rules Reducer **/
export const rulesReducer = (ruleState, ruleAction) => {
	switch(ruleAction.type){
		case "GET_RULES": {
			let newRules = ruleAction.payload || ruleState.ruleString
			return {...ruleState, ruleString: newRules}
		}
		case "MODIFY_FILTERS": {
			let filters = ruleState.filters
			const modifications = ruleAction.payload
			filters = {...filters, ...modifications}
			return {...ruleState, filters: filters}
		}
		case "COMBINE_FILTERS": {
			const filters = ruleState.filters
			let ruleString = `"${filters.query.replaceAll('"','')}" ${filters.replies} ${filters.retweets} ${filters.quotes}` // todo: handle
			// remove extra spaces, tabs, newlines, etc. -- then trim
			ruleString = ruleString.replace(/\s\s+/g, ' ').trim()
			return {...ruleState, ruleString: ruleString}
		}
		case "DECONSTRUCT_RULES_TO_FILTERS": {
			let str = ruleState.ruleString
			const results = {
				replies:   str.match("-is:reply")?.[0]    || str.match("is:reply")?.[0]    || "",
				quotes:    str.match("-is:quote")?.[0]    || str.match("is:quote")?.[0]    || "",
				retweets:  str.match("-is:retweet")?.[0]  || str.match("is:retweet")?.[0]  || "",
			};
			[results.replies, results.retweets, results.quotes].forEach(
				it => { if(it) str = str.replace(it, ""); }
			);
			results.query = str.replaceAll('"', '').trim();
			return {...ruleState, filters: {...results}}
		}
		default: return ruleState
	}
}
