import io from "socket.io-client";
import axios from "axios";

/** Stream Connections **/
export const connectToStream = (socket, dispatch) => {
	if (!socket) {
		socket = io(`${window.location.hostname}:3000`, {
			reconnection: false,
		})
		socket.on('connect', () => {
			console.log('connected to server..')
			dispatch({type: "CONNECT", payload: socket})
		})
		socket.on('tweet', (tweet) => {
			// dispatch({type: "ADD_TWEET", payload: tweet})
			dispatch({type: "PREPEND_TWEET", payload: tweet})
		})
	}
}
export const disconnectFromStream = (socket, dispatch) => {
	dispatch({type: "DISCONNECT"})
}


/** Rules Connection **/
export const viewRules = async () => { // currently unused
	let url = `http://${window.location.hostname}:3000/api/rules`
	return await axios.get(url).then( (response) => {
		return response.data && response.data[0].value
	})
}
export const getRules = (dispatch) => {
	(async () => {
		let url = `http://${window.location.hostname}:3000/api/rules`
		await axios.get(url).then( (response) => {
			if(response.data && Array.isArray(response.data)) {
				let payload = response.data?.[0].value
				dispatch({type: "GET_RULES", payload: payload})
				dispatch({type: "DECONSTRUCT_RULES_TO_FILTERS"})
			}
		})
	})()
}
export const setRules = ( dispatch, payload ) => {
	let url = `http://${window.location.hostname}:3000/api/rules`
	axios.post(url, {payload:payload}).then(res => { })
}

/** Search Tweets **/
export const loadMore = ( params ) => {
	let url = `http://${window.location.hostname}:3000/api/search`
	const { paginationOptions, stateDispatch } = params
	axios.post(url, paginationOptions).then(res => {
		if(res.data.errors) return
		const result_count  = res.data && res.data.meta && res.data.meta.result_count
		if(result_count === 0) {
			stateDispatch({type: "UPDATE_APP_MESSAGE", payload: { style: "bg-red-500",
				message:"No more tweets available in the past week"}})
			return
		}
		if(!res.data || !res.data.includes || !res.data.meta) return // last resort, TODO: show message
		// Do work
		const tweets      = res.data.data
		const users       = res.data.includes.users
		const mediaList   = res.data.includes.media
		const next_token  = res.data.meta.next_token // also newest_id, oldest_id, result_count

		const tweetObjects  = tweets.map((tweet) => {
			const api_source  = "recent"
			const tweet_id    = tweet.id
			const created_at  = tweet.created_at
			const text        = tweet.text
			const author_id   = tweet.author_id.toString()
			const user        = users.filter((it) => {return it.id.toString() === author_id})[0]
			const media       = mediaList && tweet.attachments && mediaList.filter( item => {
				return item.media_key === tweet.attachments.media_keys[0]
			})[0]
			const isSensitive = tweet.possibly_sensitive
			const isEnglish   = tweet.lang === "en"
			return { // finalizedTweet
				tweet_id,
				author_id,
				user,
				media, // TODO: issue here
				isSensitive,
				isEnglish,
				created_at,
				text,
				api_source,
			}
		})
		if(tweetObjects)  stateDispatch({type: "LOAD_PREVIOUS_TWEETS", payload: tweetObjects})
		if(next_token)    stateDispatch({type: "UPDATE_PAGINATION_TOKEN", payload: next_token})
	})
}
