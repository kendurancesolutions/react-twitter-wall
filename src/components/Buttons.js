

const OutlinedButton = ({ className, onClickFunction, text="Click",
	                themeColor = "blue", textColor="white"}) => {
	return (
		<button onClick={onClickFunction} className={className +" " +
		"bg-transparent hover:border-transparent rounded py-1/2 px-1 border font-semibold " +
		`hover:text-${textColor} text-${themeColor}-700 hover:bg-${themeColor}-500 
	border-${themeColor}-500`}>
			{text}
		</button>
)}
const FilledButton = ({ className, onClickFunction, text="Click", hoverText,
	                themeColor = "blue", textColor="white"}) => {
	return (
		<button onClick={onClickFunction} className={className +" " +
		`bg-${themeColor}-500 hover:bg-${themeColor}-700 text-${textColor} font-bold py-1/2 px-1 rounded`}
		onMouseEnter={ (ele) => {ele.target.textContent=hoverText[0]} }
		onMouseLeave={ (ele) => {ele.target.textContent=hoverText[1]} }
		>
			{text}
		</button>
)}

export const FiltersButton = ({className, onClickFunction}) => {
	return  <OutlinedButton text="Change Filters"   onClickFunction={onClickFunction}
	                themeColor="blue"    textColor="white"
									className={`inline-block text-sm w-28 bg-white ${className}`}/>
}
export const LoadMoreButton = ({onClickFunction}) => {
	return  <OutlinedButton text="Load More"    onClickFunction={onClickFunction}
	                themeColor="blue"   textColor="white"
	                className="bg-white w-full"/>
}

export const ConnectedButton = ({onClickFunction}) => {
	const starterText = 'Connected'
	return  <FilledButton text={starterText}      onClickFunction={onClickFunction}
	                themeColor="green"  textColor="white" hoverText={['Disconnect?',starterText]}
	                className="inline-block text-sm w-28"/>
}
export const DisconnectedButton = ({onClickFunction}) => {
	const starterText = 'Disconnected'
	return  <FilledButton text={starterText}   onClickFunction={onClickFunction}
	                themeColor="gray"    textColor="white" hoverText={['Connect?',starterText]}
	                className="inline-block text-sm w-28"/>
}
export const ApplyFiltersButton = ({onClickFunction}) => {
	const starterText = 'Apply Filters'
	return  <FilledButton text={starterText}      onClickFunction={onClickFunction}
	                      themeColor="green"  textColor="white"  hoverText={['Apply Filters',starterText]}
	                      className="text-sm px-2"/>
}