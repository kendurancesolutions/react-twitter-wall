import twitter_icon from "../assets/media/twitter-icon.png"
import Masonry from 'react-masonry-component' // https://github.com/eiriklv/react-masonry-component

export const TweetList = ({tweets}) => {
	const list = tweets.map( tweet => {
		return (
			<div key={tweet.tweet_id}>
				<Tweet tweet={tweet}/>
			</div>
	)})
	return (
		// TODO: Add transitions, but ensure it's added to the first item, not last -- {transitionDuration: 1_000,}
		<Masonry className={'social-media-posts w-full'} options={{columnWidth: 1,transitionDuration: 0,}}>
			{list}
		</Masonry>
)}

export const Tweet  = ({tweet}) => {
	if(tweet.api_source !== "recent") tweet = BuildTweet(tweet, 'stream')
	const user        = tweet.user
	const media       = tweet.media
	return ( (tweet.isSensitive || !tweet.isEnglish) ? <></> :
		<div className="card-container text-xs border border-blue-200 max-w-xs rounded-lg
		overflow-hidden shadow-lg mx-3 my-5">
			<img className="rounded-bl w-8 float-right" alt="twitter-icon" src={twitter_icon}/>
			<div className="card p-3 bg-white">
				<div className="card-header flex items-start">
					<div className="card-header-profile inline-block">
						<img className="rounded-lg border border-gray-400 w-10 mr-3"
							alt={user && `${user.username}-profile-pic`} src={user && user.profile_image_url}/>
					</div>
					<div className="card-header-author inline-block">
						<div className="font-bold">{user.name}</div>
						<div className="text-gray-500">@{user.username}</div>
					</div>
				</div>
				<div className="card-body">
					{media && <img className="rounded-lg border border-gray-400 w-full my-1"
					     alt={media && `${user.username}-photo`} src={media && media.url}/>}
				</div>
				<div className="card-body my-3">
					<ReplaceText text={tweet.text}/>
				</div>
				<div className="card-divider">
					<hr/>
				</div>
				<div className="card-footer">
					<div className="text-gray-500 pt-2">
						<FormattedTimestamp dateStr={tweet.created_at}/>
					</div>
				</div>
			</div>
		</div>
)}

/** Utilities **/
const ReplaceText = ({text}) => {
	const hashtag_regex = /#[a-z0-9A-Z]+/g
	const handle_regex  = /(^|[^@\w])@(\w{1,15})\b/g
	// Function used for turning all hashtags blue; Replaces all occurrences
	const _hashtags     = text && text.replace(hashtag_regex, '<span class="text-blue-500">$&</span>')
	const _handles      = _hashtags.replace(handle_regex, '<span class="text-blue-500">$&</span>');
	return <span dangerouslySetInnerHTML={{__html: _handles}} />
}
const FormattedTimestamp = ({dateStr}) => {
	const options = { weekday: 'long', year: "numeric",   month: "long", day: "numeric",
										hour: 'numeric', minute: 'numeric', hour12: true, }
	return new Intl.DateTimeFormat("en-US", options).format( new Date(dateStr) )
}
export const BuildTweet = ( data, sourceAPI ) => {
	let finalizedTweet    = {}
	switch(sourceAPI){
		case "stream":
			const api_source  = "stream"
			const tweet       = data
			const created_at  = tweet.data.created_at
			const text        = tweet.data.text
			const tweet_id    = tweet.data.id.toString()
			const author_id   = tweet.data.author_id.toString()
			const user        = tweet.includes.users.filter((it) => {return it.id.toString() === author_id})[0]
			const media       = tweet.includes.media && tweet.includes.media.length && tweet.includes.media[0]
			const isSensitive = tweet.data.possibly_sensitive
			const isEnglish   = tweet.data.lang === "en"
			finalizedTweet    = {
				tweet_id,
				author_id,
				user,
				media,
				isSensitive,
				isEnglish,
				created_at,
				text,
				api_source,
			}
			break
		case "recent":

			break
		default:
			break
	}
	return finalizedTweet
}
