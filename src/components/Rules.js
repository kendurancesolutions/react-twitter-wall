import { setRules } from "../utilities/connections";
import { ApplyFiltersButton } from "./Buttons";

export const RuleList = ( {ruleState, rulesDispatch, stateDispatch} ) => {
	return(<>
		<div className="w-full text-left p-3">
			<div className="flex justify-between">
				<Rule label={"Search Query"} formElement={<>
					<input className="w-full px-1" onChange={ (e) => {
						modifyFilters(rulesDispatch, {query:e.target.value})}} value={ruleState.filters.query} />
				</>}/>
				<Rule label={"Retweets"} formElement={<>
					<select onChange={(e) => {modifyFilters(rulesDispatch, {retweets:e.target.value})}}
					        value={ruleState.filters.retweets} className="w-full">
					  <option value="">Allow Retweets</option>
					  <option value="-is:retweet">No Retweets</option>
					</select>
				</>}/>
				<Rule label={"Replies"} formElement={<>
					<select onChange={(e) => {modifyFilters(rulesDispatch, {replies:e.target.value})}}
					        value={ruleState.filters.replies} className="w-full">
					  <option value="">Allow Replies</option>
					  <option value="-is:reply">No Replies</option>
					</select>
				</>}/>
				<Rule label={"Quote Tweets"} formElement={<>
					<select onChange={(e) => {modifyFilters(rulesDispatch, {quotes:e.target.value})}}
					        value={ruleState.filters.quotes} className="w-full">
					  <option value="">Allow Quote Tweets</option>
					  <option value="-is:quote">No Quote Tweets</option>
					</select>
				</>}/>
			</div>

			<div className="w-full ">
				<ApplyFiltersButton    onClickFunction={ () => {
					rulesDispatch({type: "COMBINE_FILTERS"})
					setRules(rulesDispatch, ruleState.ruleString)
					stateDispatch({type: "UPDATE_APP_MESSAGE", payload: {style:"bg-green-500", message:"Filters applied!"}})
				}}/>
			</div>
		</div>
</>)}

const Rule = ({label, formElement}) => {
	return (
		<div className="inline-block w-1/4">
			<label className="text-blue-800 font-bold">{label}</label>
			<div  className="border border-indigo-300">
				{formElement}
			</div>
		</div>
)}

/** Utilities **/
export const modifyFilters = (rulesDispatch, payload) => {
	if(payload) rulesDispatch({type: "MODIFY_FILTERS", payload:payload})
	rulesDispatch({type: "COMBINE_FILTERS"})
}
