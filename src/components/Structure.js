import { ConnectedButton, DisconnectedButton, FiltersButton, LoadMoreButton } from "./Buttons";
import { TweetList } from "./Tweets";
import { RuleList } from "./Rules";
import spinner from "../assets/media/spinner.gif"
import {useEffect, useState} from "react";
import {loadMore} from "../utilities/connections";

const HEADER_HEIGHT = 20

/** Header **/
export const Header = ({ state, stateDispatch, connectSocket, disconnectSocket, ruleState, rulesDispatch }) => {
	const [filtersDiv, setFiltersDiv] = useState(false)
	return (
		<header className={`z-10 fixed left-0 top-0 right-0 min-h-${HEADER_HEIGHT} 
			border-b-2 border-indigo-300 bg-white px-4 py-3
			`}>
			<div className="flex justify-between">
				<div className="inline-block w-4/5">
					<h1 className="sm:text-5xl text-blue-400 font-bold block">Twitter Tagboard</h1>
					<h1 className="sm:text-2xl text-gray-300 font-bold block">Social Media Live Stream</h1>
				</div>
				<div className="inline-block min-w-max text-right flex flex-col justify-between">
					<div>
						<span className="text-xs font-bold text-gray-700">Status: </span>
						{ state.socket  ? <ConnectedButton    onClickFunction={ disconnectSocket }/>
														: <DisconnectedButton onClickFunction={ connectSocket }/> }
					</div>
					<div>
						<FiltersButton
							onClickFunction={ () => {setFiltersDiv(!filtersDiv)} }
							className={filtersDiv ? "rounded-b-none border-b-0 z-10" : ''}
						/>
					</div>
				</div>
			</div>
			{ filtersDiv &&
			<div className="text-right">
				<div className="border border-blue-500 rounded-b rounded-tl bg-white"
				     style={{marginTop:"-2px"}}>
					<div>
						<RuleList ruleState={ruleState} rulesDispatch={rulesDispatch} stateDispatch={stateDispatch}/>
					</div>
				</div>
			</div>}
		</header>
)}

/** Body **/
export const Body = ({ socket, tweets, connectSocket }) => {
  return (
		<div id="structure-body" className={`pt-${HEADER_HEIGHT+12} pb-8`}>
			{
				socket || tweets.length ?
					!tweets.length  ? <LoadingDiv /> : <TweetList tweets={tweets}/>
													: <PlaceholderAlert connectSocket={connectSocket}/>
			}
	  </div>
)}

/** Footer **/
export const Footer = ( {tweets, paginationOptions, ruleString, stateDispatch, appMessage} ) => {
	return (
		<footer className="fixed bottom-0 left-0 right-0">
			<AppMessage appMessage={appMessage} stateDispatch={stateDispatch}/>
			<LoadMoreButton   onClickFunction={() => {
				loadMore({tweets, paginationOptions, ruleString, stateDispatch})}}>
				Load More</LoadMoreButton>
		</footer>
)}

/** Utilities **/
const PlaceholderAlert = ({connectSocket}) => {
	return(
		<div>
			<div className="table py-20 lg:px-4 mx-auto">
				<button className="p-2 bg-blue-500 items-center text-indigo-100 leading-none
								rounded-full flex lg:inline-flex" role="alert" onClick={connectSocket}>
					<span className="flex rounded-full bg-blue-800  px-2 py-1 text-xs font-bold mr-3">
						Welcome!
					</span>
					<span className="font-semibold mr-2 text-left flex-auto">
						Change status to "Connected" to get started!
					</span>
					<svg  className="fill-current opacity-75 h-4 w-4"
					      xmlns="http://www.w3.org/2000/svg"
					      viewBox="0 0 20 20">
						<path d="M12.95 10.707l.707-.707L8 4.343 6.586 5.757 10.828
										10l-4.242 4.243L8 15.657l4.95-4.95z"/>
					</svg>
				</button>
			</div>
		</div>
)}
const LoadingDiv = () => (
	<div className="text-center">
		<div className="rounded-full bg-white m-auto inline-block my-20 px-2 px-3 border border-gray-300">
			Loading...&nbsp;
			<span className="inline-block">
				<img className="h-4" src={spinner} alt="spinner" />
			</span>
		</div>
	</div>
)
const AppMessage = ({appMessage, stateDispatch}) => {
	useEffect(() => {
		appMessage && toggleView()
	},[appMessage])
	const toggleView = () => {
		setTimeout(() => {
			stateDispatch({type: "UPDATE_APP_MESSAGE", payload: {}})
		}, 3000);
	}
	return(<>
		{appMessage && appMessage.message &&
		<div id="appMessage" className="absolute bottom-10 left-1/2 transform -translate-x-1/2">
			<div className="w-full bg-gray-500 text-white text-xs font-bold rounded-xl text-center">
				<div className={`w-full ${appMessage.style} px-3 py-2 rounded-xl`}>
					{appMessage.message}
				</div>
			</div>
		</div>
		}
	</>)
}

