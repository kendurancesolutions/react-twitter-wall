import React, { useEffect, useReducer } from "react";
import { Body, Footer, Header } from "./components/Structure";
import { rulesReducer, stateReducer } from "./utilities/reducers"
import { connectToStream, disconnectFromStream, getRules } from "./utilities/connections";
import { modifyFilters } from "./components/Rules";


function App() {
  const initialState = { socket:null, tweets:[], appMessage:{}, paginationOptions:{
      pagination_token:"", max_results:10, // 10 <= max_results <= 100
    }}
  const initialRuleState = { ruleString:"", filters: {
    replies:"",   retweets:"",      quotes:"",  handle:"",  query:""},
  }
  const [state, stateDispatch]     = useReducer(stateReducer, initialState)
  const [ruleState, rulesDispatch] = useReducer(rulesReducer, initialRuleState)

  useEffect(() => {
    getRules(rulesDispatch)
    modifyFilters(rulesDispatch, null)
  }, [])

  return <div className="App h-screen">
    <Header
      connectSocket=    { () => {connectToStream(state.socket, stateDispatch)} }
      disconnectSocket= { () => {disconnectFromStream(state.socket, stateDispatch)} }
      state=            {state}
      stateDispatch=    {stateDispatch}
      ruleState=        {ruleState}
      rulesDispatch=    {rulesDispatch}
    />
    <Body
      socket=           {state.socket}
      tweets=           {state.tweets}
      connectSocket=    { () => connectToStream(state.socket, stateDispatch) }
    />
    <Footer
      tweets=           {state.tweets}
      paginationOptions={state.paginationOptions}
      ruleString=       {ruleState.ruleString}
      stateDispatch=    {stateDispatch}
      appMessage=  {state.appMessage}
    />
  </div>;
}

export default App;
