import styled from "styled-components";

/** Currently unused
		TODO: [Enhancement] Combine Tailwind with Styled-Components
 **/

export const Button = styled.button.attrs({
	className: "p-1 rounded border"})`
	
`
export const ConnectButton = styled(Button).attrs({
	className: "border-green-800 bg-green-200"})`
	
`
export const DisconnectButton  = styled(Button).attrs({
	className: "border-red-800 bg-red-200"})`
	
`
export const LoadMoreButton = styled(Button).attrs({
	className: "border-gray-800 w-full bg-gray-200"})`
	
`